/*Reza Akhavan web153 csid: 5628272 5.23.2017*/

/*create & select DB*/
create database final_81;
use final_81;

/* A database for weapons from Final Fantasy XV. This is my main table. */

CREATE TABLE finalfantasyweapons (
weapon_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
weapon_name VARCHAR(30) NOT NULL,
weapon_type VARCHAR(30) NOT NULL,
used_by VARCHAR(10) NOT NULL
);

/* Insert value */
INSERT INTO finalfantasyweapons
(weapon_name, weapon_type, used_by)
VALUES
('Engine Blade', 'Sword', 'Noctis'),
('Ultima Blade', 'Sword', 'Noctis'),
('Durandal', 'Sword', 'Noctis'),
('Hardedge', 'Greatsword', 'Gladio'),
('Dominator', 'Greatsword', 'Gladio'),
('Broadsword', 'Greatsword', 'Gladio'),
('Mythril Lance', 'Polearms', 'Ignis'),
('Flayer', 'Polearms', 'Ignis'),
('Javelin', 'Polearms', 'Ignis'),
('Valiant', 'Firearms', 'Prompto'),
('Quicksilver', 'Firearms', 'Prompto'),
('Cocytus', 'Firearms', 'Prompto');

/* My first child table, the used by list of knights. */
CREATE TABLE used_by 
(used_by_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
used_by VARCHAR(30) NOT NULL) 
AS SELECT used_by FROM finalfantasyweapons 
GROUP BY used_by ORDER BY used_by;

/*add column*/
ALTER TABLE finalfantasyweapons
ADD used_by_id INT NOT NULL AFTER weapon_id,
ADD INDEX (used_by_id);

/*set foreign key and constraint*/
UPDATE finalfantasyweapons, used_by
SET finalfantasyweapons.used_by_id = used_by.used_by_id
WHERE finalfantasyweapons.used_by = used_by.used_by;

ALTER TABLE finalfantasyweapons
ADD CONSTRAINT used_by_used_by_id_fk FOREIGN KEY (used_by_id)
REFERENCES used_by(used_by_id)
ON DELETE RESTRICT
ON UPDATE RESTRICT;


/* 2nd child table, the knight's weapon list.*/
CREATE TABLE weapon_type 
(type_id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
weapon_type VARCHAR(30) NOT NULL) 
AS SELECT weapon_type FROM finalfantasyweapons; 


/*add column*/
ALTER TABLE finalfantasyweapons
ADD weapon_type_id INT NOT NULL AFTER weapon_type,
ADD INDEX (weapon_type_id);

/*set foreign key and constraint*/
UPDATE finalfantasyweapons, weapon_type
SET finalfantasyweapons.weapon_type_id = weapon_type.type_id
WHERE finalfantasyweapons.weapon_type = weapon_type.weapon_type;

ALTER TABLE finalfantasyweapons
ADD CONSTRAINT weapon_type_type_id_fk FOREIGN KEY (weapon_type_id)
REFERENCES weapon_type(type_id)
ON DELETE RESTRICT
ON UPDATE RESTRICT;

/*Remove duplicate entries as I didn't use a distinct when importing the rows.*/
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 2;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 3;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 5;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 6;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 8;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 9;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 11;
DELETE FROM `weapon_type` WHERE `weapon_type`.`type_id` = 12;

/*View added for weapon name, type and used by.*/
CREATE VIEW finalfantasy15_weapons AS
SELECT f.weapon_name, u.used_by, w.weapon_type
FROM finalfantasyweapons f
NATURAL JOIN used_by u
NATURAL JOIN weapon_type w;

/*I sketched out potential tables until I found a 3 table solution. I chose to make a database 
of weapons used by knights in a video game and story called Final Fantasy XV. One table lists 
the weapons, their weilder and weapon type. This is my parent table. For my child tables I took the 
used_by and weapon_type and made them related to the weapons list. Now any additions to each table can 
be done without breaking the table, or risking data in any of my tables.


 Q: "What weapons does Noctis use?"
 
 A: SELECT weapon_name from vw_finalfantasyweapons
    WHERE used_by='Noctis';
 
 
Q: "What type of weapon is the Mythril Lance?"
 
 A: SELECT weapon_type from vw_finalfantasyweapons
    WHERE weapon_name='Mythril Lance';*/
 